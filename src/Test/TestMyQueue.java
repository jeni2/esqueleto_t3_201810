package Test;
import static org.junit.Assert.*;

import model.data_structures.MyQueue;



import org.junit.Before;
import org.junit.Test;

public class TestMyQueue {


	private MyQueue<Integer> cola;

	@Before
	public void setUpEscenario()
	{
		cola= new MyQueue<Integer>();
		cola.enqueue(2);
		cola.enqueue(3);
		cola.enqueue(5);
		cola.enqueue(1);
		cola.enqueue(7);
	}

	// -----------------------------------------------------------------
	// M�todos de prueba
	// -----------------------------------------------------------------

	/**
	 * enqueue
	 * dequeue
	 * size
	 * isEmpty
	 */
	@Test
	public void TestSize()
	{
		assertEquals("El tama�o de la cola no es el correcto", cola.size(), 5);
	}
	@Test
	public void testIsEmpty()
	{
		assertEquals("La cola no deber�a estar vac�a", cola.isEmpty(), false);
		cola.dequeue();
		cola.dequeue();
		cola.dequeue();
		cola.dequeue();
		cola.dequeue();

		assertTrue("La cola deber�a estar vac�a", cola.isEmpty());
	}

	@Test
	public void testEnqueue()
	{
		cola.enqueue(9);
		assertEquals("El tama�o de la cola deber�a ser 6", cola.size(), 6);

	}
	@Test
	public void testDequeue()
	{
		cola.dequeue();
		assertEquals("El tama�o de la cola deber�a haber disinu�do en 1", cola.size(), 4);

	}



}
