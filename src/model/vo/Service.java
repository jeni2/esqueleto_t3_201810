package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	private String taxiId;
	private String tripId;
	private int tripSecons;
	private double tripMiles;
	private double tripTotal;
	private String tripStartTime;
	
	public Service(String pTaxiId,String pTripId,int pTripSecons,double pTripMiles,double pTripTotal, String pTripStartTime ) {
		taxiId =pTaxiId;
		tripId=pTripId;
		tripSecons= pTripSecons;
		tripMiles= pTripMiles;
		tripTotal= pTripTotal;
		tripStartTime=pTripStartTime;
		
	}
	/** 
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return tripId;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiId;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return tripSecons;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return tripTotal;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public String getTripStartTime() {
		// TODO Auto-generated method stub
		return tripStartTime;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
