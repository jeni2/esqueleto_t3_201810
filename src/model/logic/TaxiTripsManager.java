package model.logic;

import java.io.FileReader;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.text.ParseException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.org.apache.bcel.internal.generic.NEW;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.MyLinkedList;
import model.data_structures.MyQueue;
import model.data_structures.MyStack;
import model.vo.Taxi;

import model.vo.Service;



public class TaxiTripsManager implements ITaxiTripsManager {

	private MyStack<Service> servicesPila = new MyStack<Service>();
	private MyQueue<Service> servicesCola = new MyQueue<Service>();
	// TODO
	// Definition of data model 
	
	public void loadServices(String serviceFile, String pTaxiId)
	{
		System.out.println("Inside loadServices with File:" + serviceFile);
		System.out.println("Inside loadServices with TaxiId:" + pTaxiId);
		// TODO Auto-generated method stub
		JSONParser parser =new JSONParser();
		try
		{
			JSONArray listaGson= (JSONArray)parser.parse(new FileReader(serviceFile));
			Iterator iter=  listaGson.iterator();
			int x= 0;
			while(iter.hasNext())
			{
				JSONObject o=(JSONObject) listaGson.get(x);

				String tripId= " ";
				if(o.get("trip_id") != null) {
					tripId= (String) o.get("trip_id");
				}

				String taxiId= " ";
				if(o.get("taxi_id") != null) {
					taxiId= (String) o.get("taxi_id");
				}

				int tripSeconds= 0;
				if(o.get("trip_seconds") != null) {
					tripSeconds= Integer.parseInt((String) o.get("trip_seconds"));
				}

				double tripMiles= 0;
				if(o.get("trip_miles") != null) {
					tripMiles= Double.parseDouble((String) o.get("trip_miles"));
				}
				double tripTotal= 0;
				if(o.get("trip_total") != null) {
					tripTotal= Double.parseDouble((String) o.get("trip_total"));
				}

				String FechaInicioServicio="";
				String HoraInicioServicio="";
				String inicio ="";
				if(o.get("trip_start_timestamp") != null)
				{
					inicio = (String) o.get("trip_start_timestamp");
					HoraInicioServicio= inicio.substring(inicio.indexOf("T"));
					FechaInicioServicio =inicio.substring(0,inicio.indexOf("T") );



				}
				
				Service serviciox = null;
				
				serviciox = new Service(taxiId, tripId, tripSeconds, tripMiles, tripTotal, inicio);

				
				
				if(serviciox.getTaxiId().equals(pTaxiId))
				{			
					servicesCola.enqueue(serviciox);
					servicesPila.push(serviciox);
				}




				x++;

				iter.next();
			}	





		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		System.out.println(servicesCola.size());
		System.out.println(servicesPila.size());

	}

	@Override
	public int [] servicesInInverseOrder() {
		// TODO Auto-generated method stub
		System.out.println("Inside servicesInInverseOrder");
		int [] resultado = new int[2];
		// TODO Auto-generated method stub
		//		System.out.println("Inside servicesInOrder");
		//		int [] resultado = new int[2];
		//		return resultado;

		int tamanio= servicesPila.size();
		int NoOrdenados=0;

		Service anterior= servicesPila.pop();
		while(!servicesPila.isEmpty())
		{
			Service actual= servicesPila.pop();
			SimpleDateFormat formato= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

			Date fecha1 = null;
			Date fecha2 = null;
			try {
				fecha1 = (Date) formato.parse(anterior.getTripStartTime());
				fecha2= (Date) formato.parse(actual.getTripStartTime());
			} 
			catch (ParseException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			
				
			
				// TODO Auto-generated catch block
			
			}
			if(!fecha1.before(fecha2))
			{
				NoOrdenados++;
			}
			else
			{
				anterior=actual;
			}
		}

		System.out.println("Inside servicesInOrder");
		
		resultado[0]= tamanio-NoOrdenados;
		resultado[1]= NoOrdenados;
		return resultado;
	
	}

	@Override
	public int [] servicesInOrder() {
		// TODO Auto-generated method stub
		//		System.out.println("Inside servicesInOrder");
		//		int [] resultado = new int[2];
		//		return resultado;

		int tamanio= servicesCola.size();
		int NoOrdenados=0;

		Service anterior= servicesCola.dequeue();
		while(!servicesCola.isEmpty())
		{
			Service actual= servicesCola.dequeue();
			SimpleDateFormat formato= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

			Date fecha1 = null;
			Date fecha2 = null;
			try {
				fecha1 = (Date) formato.parse(anterior.getTripStartTime());
				fecha2= (Date) formato.parse(actual.getTripStartTime());
			} 
			catch (ParseException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			
				
			
				// TODO Auto-generated catch block
			
			}
			if(!fecha1.before(fecha2))
			{
				NoOrdenados++;
			}
			else
			{
				anterior=actual;
			}
		}

		System.out.println("Inside servicesInOrder");
		int [] resultado = new int[2];
		resultado[0]= tamanio-NoOrdenados;
		resultado[1]= NoOrdenados;
		return resultado;
	}
}


