package model.data_structures;

import java.util.EmptyStackException;

public class MyStack<T extends Comparable<T>> implements IStack<T> {

	private Node<T> topStack;
	private int size = 0;

	@Override
	/**
	 * agrega al principio de la pila
	 */
	public void push(T item) {
		
		// TODO Auto-generated method stub
		Node <T> newNode = new Node <T> (item);
		if (topStack == null )
		topStack = newNode;
		else {
		newNode.setNextNode(topStack);
		topStack = newNode;
		}
		size++;
	}

	/**
	 * elimina el primer elemento de la pila
	 */
	@Override
	public T pop() {
		// TODO Auto-generated method stub
		if (topStack == null )
			throw new EmptyStackException();
			T elem = topStack.getItem();
			Node <T> nexTop = topStack.getNext();
			topStack.setNextNode(null);
			topStack = nexTop;
			size--;
			return elem;

	}
	/**
	 * dice si esta vacia la pila
	 */

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return (size == 0);
	}
	/**
	 * retorna el tama�o de la pila
	 * @return
	 */
	public int size() {
		// TODO Auto-generated method stub
		return   size;
	}

}
